library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity arith32Bit is
  Port ( num1: in std_logic_vector(31 downto 0);
         num2: in std_logic_vector(31 downto 0);
         operator: in std_logic_vector(2 downto 0);
         result: out std_logic_vector(31 downto 0));
end arith32Bit;

architecture Behavioral of arith32Bit is

begin
  process
  begin
    case operator is
      when "000" => result <= std_logic_vector(signed(num1)+signed(num2));
      when "001" => result <= std_logic_vector(signed(num1)-signed(num2));
      when "010" => result <= std_logic_vector(signed(num2)-signed(num1));
      when "011" => result <= std_logic_vector(signed(num1(15 downto 0))*signed(num2(15 downto 0)));
      when "100" => result <= std_logic_vector(signed(num1)/signed(num2));
      when "101" => result <= std_logic_vector(signed(num2)/signed(num1));
      when "110" => result <= std_logic_vector(signed(num1)mod signed(num2));
      when "111" => result <= std_logic_vector(signed(num2)mod signed(num1));
      when others => result <= (others=>'0');
    end case;
  end process;

end Behavioral;

connect -url tcp:127.0.0.1:3121
source /home/sil/Documents/fpga/EmbeddedFPGA/ex23/ex23.sdk/design_1_wrapper_hw_platform_0/ps7_init.tcl
targets -set -filter {jtag_cable_name =~ "Avnet MiniZed V1 1234-oj1A" && level==0} -index 1
fpga -file /home/sil/Documents/fpga/EmbeddedFPGA/ex23/ex23.sdk/design_1_wrapper_hw_platform_0/design_1_wrapper.bit
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Avnet MiniZed V1 1234-oj1A"} -index 0
loadhw -hw /home/sil/Documents/fpga/EmbeddedFPGA/ex23/ex23.sdk/design_1_wrapper_hw_platform_0/system.hdf -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Avnet MiniZed V1 1234-oj1A"} -index 0
stop
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Avnet MiniZed V1 1234-oj1A"} -index 0
rst -processor
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Avnet MiniZed V1 1234-oj1A"} -index 0
profile -freq 100000 -scratchaddr 0x10000000
dow /home/sil/Documents/fpga/EmbeddedFPGA/ex23/ex23.sdk/profileMath/Debug/profileMath.elf
configparams force-mem-access 0
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Avnet MiniZed V1 1234-oj1A"} -index 0
set bpid [bpadd -addr &_exit]
con -block
profile -out /home/sil/Documents/fpga/EmbeddedFPGA/ex23/ex23.sdk/profileMath/Debug/gmon.out
bpremove $bpid
con

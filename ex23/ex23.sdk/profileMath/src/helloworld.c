/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

//#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xil_types.h"
#include "mult16BIP.h"
#include "xil_io.h"

#define MULTBASEADDR 0x43c00000

//#define SOFTWARE

void swFunc() {
	//xil_printf("In sw function\n\r");
	int16_t num1 = 2000;
	int16_t num2 = 13001;
	int32_t outNum = num1 * num2;
	//xil_printf("Output number is: %d\n\r", outNum);
}

void hwFunc() {
	xil_printf("In hw function\n\r");
	int16_t num1 = 2000;
	int16_t num2 = 13001;
	MULT16BIP_mWriteReg(MULTBASEADDR, 4, (num1<<15));
	MULT16BIP_mWriteReg(MULTBASEADDR, 4, (num2));
	int32_t outNum  = MULT16BIP_mReadReg(MULTBASEADDR,0);
	xil_printf("Output number is: %d\n\r", outNum);
}

int main()
{
    init_platform();
#ifdef SOFTWARE
    swFunc();
#else
    hwFunc();
#endif
    //print("Hello World\n\r");

    cleanup_platform();
    return 0;
}

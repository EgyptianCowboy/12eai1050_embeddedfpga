library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PWMGen is

  port (
      toggle_count : in  std_logic_vector(31 downto 0);        -- Input toggle count
      total_count  : in  std_logic_vector (31 downto 0);       -- Input total
                                                               -- clock divider
      inClk        : in  std_logic;                            -- Input clock
      rst          : in  std_logic;                            -- Input reset, active high
      outPwm       : out std_logic;                            -- Non inverted PWM output
      outNPwm      : out std_logic);                           -- Inverted PWM output


end entity PWMGen;


architecture behavorial of PWMGen is
    signal counter : std_logic_vector(31 downto 0) := (others => '0');
    signal pwmBuf  : std_logic := '1';

begin  -- architecture behavorial
    -- purpose: This process generates a pwm signal with a desired frequency and duty cycle
    -- type   : sequential
    -- inputs : inClk, rst
    -- outputs: pwmBuf
    pwmGenerator: process (inClk, rst) is
    begin  -- process pwmGenerator
        if rst = '1' then                   -- asynchronous reset (active high)
            counter <= (others => '0');
            pwmBuf <= '0';
        elsif inClk'event and inClk = '1' then  -- rising clock edge
            if(counter = total_count) then
                pwmBuf <= '1';
                counter <= (others => '0');
            elsif(counter = toggle_count) then
                pwmBuf <= '0';
                counter <= std_logic_vector(unsigned(counter) + 1);
            else
                counter <= std_logic_vector(unsigned(counter) + 1);
            end if;
            end if;
    end process pwmGenerator;
    outNPwm <= not(pwmBuf);
    outPwm  <= pwmBuf;
end architecture behavorial;

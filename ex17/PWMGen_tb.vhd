library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------

entity PWMGen_tb is

end entity PWMGen_tb;

-------------------------------------------------------------------------------

architecture behavorial of PWMGen_tb is
    -- component ports
    signal DT        : std_logic_vector(7 downto 0);
    signal pwmFreq   : std_logic_vector (31 downto 0);
    signal clkFreq   : std_logic_vector (31 downto 0);
    signal inClk     : std_logic := '0';
    signal rst       : std_logic := '1';
    signal outPwm    : std_logic;
    signal outNPwm   : std_logic;
    signal cTFreq    : std_logic_vector(28 downto 0) := "11101110011010110010100000000";
    signal pTFreq    : std_logic_vector(27 downto 0) := "0000111001101011001010000000";
    signal simClk    : std_logic := '0';
    signal numCycles : integer := 1200;

begin  -- architecture behavorial

  -- component instantiation
  uut: entity work.PWMGen
    port map (
      DT      => DT,
      pwmFreq => pwmFreq,
      clkFreq => clkFreq,
      inClk   => inClk,
      rst     => rst,
      outPwm  => outPwm,
      outNPwm => outNPwm);

  inClk <= simClk;

  stimProc: process
  begin
      wait for 10 ns;
      pwmFreq(27 downto 0) <= pTFreq;
      pwmFreq(31 downto 28) <= (others => '0');
      --pwmFreq <= (1 downto 0 => '1', others => '0');
      DT(7 downto 0) <= X"05";
      --pwmFreq(7 downto 0) <= pTFreq;
      --pwmFreq(31 downto 8) <= (others => '0');
      clkFreq(28 downto 0) <= cTFreq;
      clkFreq(31 downto 29) <= (others => '0');
      for i in 1 to numCycles loop
          simClk <= not simClk;
          wait for 1 ns;
          simClk <= not simClk;
          wait for 1 ns;
      end loop;
      wait;
  end process;


end architecture behavorial;

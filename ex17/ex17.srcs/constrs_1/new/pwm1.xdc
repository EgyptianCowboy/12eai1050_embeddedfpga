set_property PACKAGE_PIN E13 [get_ports outPwm_0]
set_property PACKAGE_PIN E12 [get_ports outNPwm_0]
set_property IOSTANDARD LVCMOS33 [get_ports outNPwm_0]
set_property IOSTANDARD LVCMOS33 [get_ports outPwm_0]

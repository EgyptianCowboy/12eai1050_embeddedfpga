library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SEG7 is
  Port (
    inVec: in STD_LOGIC_VECTOR(0 to 3);
    outSeg: out STD_LOGIC_VECTOR(0 to 7)
    );
end SEG7;

architecture Behavioral of SEG7 is
begin

  with inVec select
    outSeg <=
    "00111111"  when "0000",
    "00001001"  when "0001",
    "10100111"  when "0010",
    "10001111"  when "0011",
    "10011001"  when "0100",
    "10011110"  when "0101",
    "10111110"  when "0110",
    "00001011"  when "0111",
    "10111111"  when "1000",
    "10011111"  when "1001",
    "01110111"  when "1010",
    "01111100"  when "1011",
    "00111001"  when "1100",
    "01011110"  when "1101",
    "01111001"  when "1110",
    "01110001"  when "1111",
    "01000000"  when others;


end Behavioral;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity statemachine is
    Port (reset : in STD_LOGIC;
          ledG : out STD_LOGIC;
          ledR : out STD_LOGIC);
end statemachine;

architecture Behavioral of statemachine is
    component vhdlnoclk
        port (clk65MHz : out std_logic);
    end component;

    type state_type is (GREEN, ORANGE, RED);

    signal state, next_state : state_type := GREEN;
    signal int_clk     : STD_LOGIC;
    signal timer       : STD_LOGIC_VECTOR(31 downto 0);
    signal timerRED    : STD_LOGIC_VECTOR(31 downto 0) := "00000111101111111010010010000000";
    signal timerORANGE : STD_LOGIC_VECTOR(31 downto 0) := "00000011110111111101001001000000";
begin
    clock: vhdlnoclk port map(int_clk);

    sync_state : process(int_clk, reset)
    begin
        if(reset = '1') then
            state <= RED;
            timer <= (others => '0');
        elsif(int_clk'event and int_clk = '1') then
            -- state <= next_state;
            case state is
            when RED =>
                if(timer < timerRED) then
                    timer <= std_logic_vector(unsigned(timer) + 1);
                else
                    timer <= (others => '0');
                    state <= GREEN;
                end if;
            when ORANGE =>
                if(timer < timerORANGE) then
                    timer <= std_logic_vector(unsigned(timer) + 1);
                else
                    timer <= (others => '0');
                    state <= RED;
                end if;
            when GREEN =>
                if(timer < timerRED) then
                    timer <= std_logic_vector(unsigned(timer) + 1);
                else
                    timer <= (others => '0');
                    state <= ORANGE;
                end if;
            when others =>
                state <= GREEN;
        end case;
        end if;
    end process;

    output_decode : process(state)
    begin
        case state is
            when RED =>
                ledR <= '1';
                ledG <= '0';
            when GREEN =>
                ledR <= '0';
                ledG <= '1';
            when ORANGE =>
                ledR <= '1';
                ledG <= '1';
            when others =>
                ledR <= '1';
                ledG <= '0';
        end case;
    end process;

end Behavioral;

connect -url tcp:127.0.0.1:3121
set bp_5_5_0 [bpadd -target-id all -ct-input {0} -ct-output {24 25 26 27}]
set bp_5_5_1 [bpadd -target-id all -ct-input {24 25 26 27} -ct-output {0}]
source /home/sil/Documents/fpga/EmbeddedFPGA/ex22/ex22.sdk/design_1_wrapper_hw_platform_0/ps7_init.tcl
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Avnet MiniZed V1 1234-oj1A"} -index 0
loadhw -hw /home/sil/Documents/fpga/EmbeddedFPGA/ex22/ex22.sdk/design_1_wrapper_hw_platform_0/system.hdf -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Avnet MiniZed V1 1234-oj1A"} -index 0
stop
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Avnet MiniZed V1 1234-oj1A"} -index 0
rst -processor
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Avnet MiniZed V1 1234-oj1A"} -index 0
dow /home/sil/Documents/fpga/EmbeddedFPGA/ex22/ex22.sdk/ilaVioTest/Debug/ilaVioTest.elf
configparams force-mem-access 0
bpadd -addr &main

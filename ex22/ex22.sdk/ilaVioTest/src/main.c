#include "xil_printf.h"
#include "xil_io.h"
#include "mult16BIP.h"
#include <inttypes.h>
#include "sleep.h"
#include "xgpio.h"

#define AXI_GPIO_DEVICE_ID      XPAR_AXI_GPIO_0_DEVICE_ID
#define LEDG_CHANNEL 1
#define LEDR_CHANNEL 2

#define MATHBASEADDR 0x43c00000

static u32 ledR = 0x00000000;
static u32 ledG = 0x00000000;

static XGpio GpioPL;

int main () {
	int32_t out= 0;
	xil_printf("Starting\n\r");
	XGpio_Initialize(&GpioPL, AXI_GPIO_DEVICE_ID);
	XGpio_SetDataDirection(&GpioPL, LEDG_CHANNEL, 0x00000000);
	XGpio_SetDataDirection(&GpioPL, LEDR_CHANNEL, 0x00000000);

	XGpio_DiscreteWrite(&GpioPL, LEDG_CHANNEL, ledR);
	XGpio_DiscreteWrite(&GpioPL, LEDR_CHANNEL, ledG);

	Xil_Out32(MATHBASEADDR+4, 0x01110012);
	out = Xil_In32(MATHBASEADDR+0);
	xil_printf("output is %"PRIi32"\n\r", out);
	if(out>0x100) {
		XGpio_DiscreteWrite(&GpioPL, LEDG_CHANNEL, 0x01);
		XGpio_DiscreteWrite(&GpioPL, LEDR_CHANNEL, 0x01);
	} else {
		XGpio_DiscreteWrite(&GpioPL, LEDR_CHANNEL, 0x01);
	}
	//sleep_A9(1);
	xil_printf("Ending\n\r");
	return 0;
}

// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Thu Jan 24 15:26:46 2019
// Host        : x220 running 64-bit Debian GNU/Linux buster/sid
// Command     : write_verilog -force -mode synth_stub
//               /home/sil/Documents/fpga/EmbeddedFPGA/ex22/ex22.srcs/sources_1/bd/design_1/ip/design_1_vio_0_0/design_1_vio_0_0_stub.v
// Design      : design_1_vio_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z007sclg225-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "vio,Vivado 2018.2" *)
module design_1_vio_0_0(clk, probe_in0, probe_out0, probe_out1)
/* synthesis syn_black_box black_box_pad_pin="clk,probe_in0[31:0],probe_out0[15:0],probe_out1[15:0]" */;
  input clk;
  input [31:0]probe_in0;
  output [15:0]probe_out0;
  output [15:0]probe_out1;
endmodule

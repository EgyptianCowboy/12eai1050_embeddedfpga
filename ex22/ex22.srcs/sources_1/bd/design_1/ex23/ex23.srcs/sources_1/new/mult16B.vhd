----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 01/11/2019 08:58:38 AM
-- Design Name:
-- Module Name: mult16B - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mult16B is
    Port (num1, num2, hw_num1, hw_num2 : in  std_logic_vector(15 downto 0);
          num_out, hw_num_out          : out std_logic_vector(31 downto 0));
end mult16B;

architecture Behavioral of mult16B is

begin
    num_out <= std_logic_vector(signed(num1)*signed(num2));
    hw_num_out <= std_logic_vector(signed(hw_num1)*signed(hw_num2));

end Behavioral;

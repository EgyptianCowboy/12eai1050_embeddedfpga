/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
//#include <stdlib.h>
//#include <xil_types.h>
#include <inttypes.h>
#include "platform.h"
#include "xil_printf.h"
#include "xgpio.h"
#include "xstatus.h"
#include "xplatform_info.h"

#define GPIO_DEVICE_ID		XPAR_XGPIOPS_0_DEVICE_ID
#define LEDR_CHANNEL 1
#define LEDG_CHANNEL 2

static u32 ledR = 0x00000000;
static u32 ledG = 0x00000000;
XGpio Gpio;	/* The driver instance for GPIO Device. */

int main()
{
    init_platform();
    uint8_t Status;
    uint8_t num;

    print("Hello World\n\r");

    //XGpio_Config *ConfigPtr;

    /* Initialize the GPIO driver. */
    //ConfigPtr = XGpio_LookupConfig(GPIO_DEVICE_ID);
    Status = XGpio_Initialize(&Gpio, GPIO_DEVICE_ID);
    if (Status != XST_SUCCESS) {
	return XST_FAILURE;
    }

    /* Run the Output Example. */
    XGpio_SetDataDirection(&Gpio, LEDR_CHANNEL, 0x00000000);
    XGpio_SetDataDirection(&Gpio, LEDG_CHANNEL, 0x00000000);

    XGpio_DiscreteWrite(&Gpio, LEDR_CHANNEL, ledR);
    XGpio_DiscreteWrite(&Gpio, LEDG_CHANNEL, ledG);

    while(1) {
	print("Enter a number between 1 and 3\n\r");
	scanf("%" SCNu8, &num);
	if(num & 0x01) {
	    ledR = 0xFFFFFFFF;
	} else {
	    ledR = 0X00000000;
	}

	if((num >> 1)& 0x01) {
	    ledG = 0xFFFFFFFF;
	} else {
	    ledG = 0X00000000;
	}
	XGpio_DiscreteWrite(&Gpio, LEDR_CHANNEL, ledR);
	XGpio_DiscreteWrite(&Gpio, LEDG_CHANNEL, ledG);
	xil_printf("The number is %d\n\r", num);
    }
    cleanup_platform();
    return 0;
}

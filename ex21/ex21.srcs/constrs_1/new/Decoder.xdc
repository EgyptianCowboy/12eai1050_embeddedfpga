set_property IOSTANDARD LVCMOS33 [get_ports rt1_0]
set_property IOSTANDARD LVCMOS33 [get_ports rt2_0]

set_property IOSTANDARD LVCMOS33 [get_ports rot_A_0]
set_property IOSTANDARD LVCMOS33 [get_ports rot_B_0]
set_property IOSTANDARD LVCMOS33 [get_ports rot_SW_0]
set_property PACKAGE_PIN R8 [get_ports rot_A_0]
set_property PACKAGE_PIN P8 [get_ports rot_B_0]
set_property PACKAGE_PIN P9 [get_ports rot_SW_0]

----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 01/08/2019 02:44:29 PM
-- Design Name:
-- Module Name: rotEncoder - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rotEncoder is
    Port (
        clk    : in std_logic;
        rt1    : in std_logic;
        rt2    : in std_logic;
        detent : out std_logic;
        dir    : out std_logic
 );
end rotEncoder;

architecture Behavioral of rotEncoder is
    signal rt1_in       : std_logic;
    signal rt2_in       : std_logic;
    signal rt_in        : std_logic_vector(1 downto 0);
    signal rt_q1       : std_logic;
    signal rt_q2       : std_logic;
    signal delay_q1     : std_logic;
    signal rt_event    : std_logic;
    signal rt_left     : std_logic;
    signal int_det      : std_logic;
    signal int_dir      : std_logic;

begin
-- purpose: filter and debounce
-- type   : sequential
-- inputs : clk, rt1, rt2
-- outputs: dir
rotary_filter: process (clk) is
begin -- process setLeftRight
    if clk'event and clk = '1' then
        rt1_in <= rt1;
        rt2_in <= rt2;

        rt_in <= rt1_in & rt2_in;
        case rt_in is
            when "00"   => rt_q1 <= '0';
                           rt_q2 <= rt_q2;

            when "01"   => rt_q1 <= rt_q1;
                           rt_q2 <= '0';

            when "10"   => rt_q1 <= rt_q1;
                           rt_q2 <= '1';

            when "11"   => rt_q1 <= '1';
                           rt_q2 <= rt_q2;

            when others => rt_q1 <= rt_q1;
                           rt_q2 <= rt_q2;
        end case;
    end if;

end process rotary_filter;

direction: process(clk)
begin
    if clk'event and clk = '1' then
        delay_q1 <= rt_q1;
        if rt_q1 = '1' and delay_q1 = '0' then
            rt_left <= rt_q2;
            rt_event <= '1';
        else
            rt_left <= rt_left;
            rt_event <= '0';
        end if;
    end if;
end process direction;

output_sig: process(clk)
begin
    if clk'event and clk = '1' then
        if rt_event = '1' then
            if rt_left = '1' then
                int_dir <= '1';
            else
                int_dir <= '0';
            end if;
            int_det <= '1';
        else
            int_det <= '0';
        end if;
        detent <= int_det;
    end if;
end process output_sig;
dir <= int_dir;
end Behavioral;

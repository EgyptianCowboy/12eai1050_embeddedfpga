----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 01/08/2019 02:44:29 PM
-- Design Name:
-- Module Name: rotEncoder - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rotEncoder is
    Port (
        clk : in std_logic;
        rt  : in std_logic_vector(1 downto 0);
        oDD : out std_logic_vector(1 downto 0));
end rotEncoder;

architecture Behavioral of rotEncoder is
    signal state : std_logic_vector(1 downto 0) := "00";


begin
-- purpose: set oDD
-- type   : sequential
-- inputs : clk, rt
-- outputs: oDD
setDir: process (clk) is
begin -- process setLeftRight
    if clk'event and clk='1' then
        case state is
            when "00" =>
                case rt is
                    when "00" => oDD <= "00";
                    when "01" => oDD <= "01"; state <= "11";
                    when "10" => oDD <= "10"; state <= "01";
                    when others => oDD <= "00";
                end case;
            when "01" =>
                case rt is
                    when "00" => oDD <= "01"; state <= "00";
                    when "10" => oDD <= "00";
                    when "11" => oDD <= "10"; state <= "10";
                    when others => oDD <= "00";
                end case;
            when "10" =>
                case rt is
                    when "01" => oDD <= "10"; state <= "11";
                    when "10" => oDD <= "01"; state <= "01";
                    when "11" => oDD <= "00";
                    when others => oDD <= "00";
                end case;
            when "11" =>
                case rt is
                    when "00" => oDD <= "10"; state <= "00";
                    when "01" => oDD <= "00";
                    when "11" => oDD <= "01"; state <= "10";
                    when others => oDD <= "00";
                end case;
            when others => null;
        end case;
    end if;
end process setDir;
end Behavioral;

// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Wed Jan  9 14:41:25 2019
// Host        : x220 running 64-bit Debian GNU/Linux buster/sid
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_rotary_decoder_0_0_sim_netlist.v
// Design      : design_1_rotary_decoder_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z007sclg225-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce
   (sw_deb,
    SW_rise_latch_q_reg,
    s00_axi_aclk,
    SW_rise_latch_q,
    sw_deb_delay,
    axi_rvalid_reg,
    axi_arready_reg,
    s00_axi_arvalid,
    rot_SW);
  output sw_deb;
  output SW_rise_latch_q_reg;
  input s00_axi_aclk;
  input SW_rise_latch_q;
  input sw_deb_delay;
  input axi_rvalid_reg;
  input axi_arready_reg;
  input s00_axi_arvalid;
  input rot_SW;

  wire SW_rise_latch_q;
  wire SW_rise_latch_q_reg;
  wire axi_arready_reg;
  wire axi_rvalid_reg;
  wire \counter_out[0]_i_4_n_0 ;
  wire [20:20]counter_out_reg;
  wire \counter_out_reg[0]_i_3_n_0 ;
  wire \counter_out_reg[0]_i_3_n_1 ;
  wire \counter_out_reg[0]_i_3_n_2 ;
  wire \counter_out_reg[0]_i_3_n_3 ;
  wire \counter_out_reg[0]_i_3_n_4 ;
  wire \counter_out_reg[0]_i_3_n_5 ;
  wire \counter_out_reg[0]_i_3_n_6 ;
  wire \counter_out_reg[0]_i_3_n_7 ;
  wire \counter_out_reg[12]_i_1_n_0 ;
  wire \counter_out_reg[12]_i_1_n_1 ;
  wire \counter_out_reg[12]_i_1_n_2 ;
  wire \counter_out_reg[12]_i_1_n_3 ;
  wire \counter_out_reg[12]_i_1_n_4 ;
  wire \counter_out_reg[12]_i_1_n_5 ;
  wire \counter_out_reg[12]_i_1_n_6 ;
  wire \counter_out_reg[12]_i_1_n_7 ;
  wire \counter_out_reg[16]_i_1_n_0 ;
  wire \counter_out_reg[16]_i_1_n_1 ;
  wire \counter_out_reg[16]_i_1_n_2 ;
  wire \counter_out_reg[16]_i_1_n_3 ;
  wire \counter_out_reg[16]_i_1_n_4 ;
  wire \counter_out_reg[16]_i_1_n_5 ;
  wire \counter_out_reg[16]_i_1_n_6 ;
  wire \counter_out_reg[16]_i_1_n_7 ;
  wire \counter_out_reg[20]_i_1_n_7 ;
  wire \counter_out_reg[4]_i_1_n_0 ;
  wire \counter_out_reg[4]_i_1_n_1 ;
  wire \counter_out_reg[4]_i_1_n_2 ;
  wire \counter_out_reg[4]_i_1_n_3 ;
  wire \counter_out_reg[4]_i_1_n_4 ;
  wire \counter_out_reg[4]_i_1_n_5 ;
  wire \counter_out_reg[4]_i_1_n_6 ;
  wire \counter_out_reg[4]_i_1_n_7 ;
  wire \counter_out_reg[8]_i_1_n_0 ;
  wire \counter_out_reg[8]_i_1_n_1 ;
  wire \counter_out_reg[8]_i_1_n_2 ;
  wire \counter_out_reg[8]_i_1_n_3 ;
  wire \counter_out_reg[8]_i_1_n_4 ;
  wire \counter_out_reg[8]_i_1_n_5 ;
  wire \counter_out_reg[8]_i_1_n_6 ;
  wire \counter_out_reg[8]_i_1_n_7 ;
  wire \counter_out_reg_n_0_[0] ;
  wire \counter_out_reg_n_0_[10] ;
  wire \counter_out_reg_n_0_[11] ;
  wire \counter_out_reg_n_0_[12] ;
  wire \counter_out_reg_n_0_[13] ;
  wire \counter_out_reg_n_0_[14] ;
  wire \counter_out_reg_n_0_[15] ;
  wire \counter_out_reg_n_0_[16] ;
  wire \counter_out_reg_n_0_[17] ;
  wire \counter_out_reg_n_0_[18] ;
  wire \counter_out_reg_n_0_[19] ;
  wire \counter_out_reg_n_0_[1] ;
  wire \counter_out_reg_n_0_[2] ;
  wire \counter_out_reg_n_0_[3] ;
  wire \counter_out_reg_n_0_[4] ;
  wire \counter_out_reg_n_0_[5] ;
  wire \counter_out_reg_n_0_[6] ;
  wire \counter_out_reg_n_0_[7] ;
  wire \counter_out_reg_n_0_[8] ;
  wire \counter_out_reg_n_0_[9] ;
  wire counter_set;
  wire \flipflops[0]_i_1_n_0 ;
  wire \flipflops_reg_n_0_[0] ;
  wire p_0_in;
  wire result_i_1_n_0;
  wire rot_SW;
  wire s00_axi_aclk;
  wire s00_axi_arvalid;
  wire sel;
  wire sw_deb;
  wire sw_deb_delay;
  wire [3:0]\NLW_counter_out_reg[20]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_counter_out_reg[20]_i_1_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hBA00BABABABABABA)) 
    SW_rise_latch_q_i_1
       (.I0(SW_rise_latch_q),
        .I1(sw_deb_delay),
        .I2(sw_deb),
        .I3(axi_rvalid_reg),
        .I4(axi_arready_reg),
        .I5(s00_axi_arvalid),
        .O(SW_rise_latch_q_reg));
  LUT2 #(
    .INIT(4'h6)) 
    \counter_out[0]_i_1 
       (.I0(p_0_in),
        .I1(\flipflops_reg_n_0_[0] ),
        .O(counter_set));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_out[0]_i_2 
       (.I0(counter_out_reg),
        .O(sel));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_out[0]_i_4 
       (.I0(\counter_out_reg_n_0_[0] ),
        .O(\counter_out[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[0] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[0]_i_3_n_7 ),
        .Q(\counter_out_reg_n_0_[0] ),
        .R(counter_set));
  CARRY4 \counter_out_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\counter_out_reg[0]_i_3_n_0 ,\counter_out_reg[0]_i_3_n_1 ,\counter_out_reg[0]_i_3_n_2 ,\counter_out_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_out_reg[0]_i_3_n_4 ,\counter_out_reg[0]_i_3_n_5 ,\counter_out_reg[0]_i_3_n_6 ,\counter_out_reg[0]_i_3_n_7 }),
        .S({\counter_out_reg_n_0_[3] ,\counter_out_reg_n_0_[2] ,\counter_out_reg_n_0_[1] ,\counter_out[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[10] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[8]_i_1_n_5 ),
        .Q(\counter_out_reg_n_0_[10] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[11] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[8]_i_1_n_4 ),
        .Q(\counter_out_reg_n_0_[11] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[12] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[12]_i_1_n_7 ),
        .Q(\counter_out_reg_n_0_[12] ),
        .R(counter_set));
  CARRY4 \counter_out_reg[12]_i_1 
       (.CI(\counter_out_reg[8]_i_1_n_0 ),
        .CO({\counter_out_reg[12]_i_1_n_0 ,\counter_out_reg[12]_i_1_n_1 ,\counter_out_reg[12]_i_1_n_2 ,\counter_out_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_out_reg[12]_i_1_n_4 ,\counter_out_reg[12]_i_1_n_5 ,\counter_out_reg[12]_i_1_n_6 ,\counter_out_reg[12]_i_1_n_7 }),
        .S({\counter_out_reg_n_0_[15] ,\counter_out_reg_n_0_[14] ,\counter_out_reg_n_0_[13] ,\counter_out_reg_n_0_[12] }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[13] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[12]_i_1_n_6 ),
        .Q(\counter_out_reg_n_0_[13] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[14] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[12]_i_1_n_5 ),
        .Q(\counter_out_reg_n_0_[14] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[15] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[12]_i_1_n_4 ),
        .Q(\counter_out_reg_n_0_[15] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[16] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[16]_i_1_n_7 ),
        .Q(\counter_out_reg_n_0_[16] ),
        .R(counter_set));
  CARRY4 \counter_out_reg[16]_i_1 
       (.CI(\counter_out_reg[12]_i_1_n_0 ),
        .CO({\counter_out_reg[16]_i_1_n_0 ,\counter_out_reg[16]_i_1_n_1 ,\counter_out_reg[16]_i_1_n_2 ,\counter_out_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_out_reg[16]_i_1_n_4 ,\counter_out_reg[16]_i_1_n_5 ,\counter_out_reg[16]_i_1_n_6 ,\counter_out_reg[16]_i_1_n_7 }),
        .S({\counter_out_reg_n_0_[19] ,\counter_out_reg_n_0_[18] ,\counter_out_reg_n_0_[17] ,\counter_out_reg_n_0_[16] }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[17] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[16]_i_1_n_6 ),
        .Q(\counter_out_reg_n_0_[17] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[18] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[16]_i_1_n_5 ),
        .Q(\counter_out_reg_n_0_[18] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[19] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[16]_i_1_n_4 ),
        .Q(\counter_out_reg_n_0_[19] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[1] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[0]_i_3_n_6 ),
        .Q(\counter_out_reg_n_0_[1] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[20] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[20]_i_1_n_7 ),
        .Q(counter_out_reg),
        .R(counter_set));
  CARRY4 \counter_out_reg[20]_i_1 
       (.CI(\counter_out_reg[16]_i_1_n_0 ),
        .CO(\NLW_counter_out_reg[20]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_counter_out_reg[20]_i_1_O_UNCONNECTED [3:1],\counter_out_reg[20]_i_1_n_7 }),
        .S({1'b0,1'b0,1'b0,counter_out_reg}));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[2] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[0]_i_3_n_5 ),
        .Q(\counter_out_reg_n_0_[2] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[3] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[0]_i_3_n_4 ),
        .Q(\counter_out_reg_n_0_[3] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[4] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[4]_i_1_n_7 ),
        .Q(\counter_out_reg_n_0_[4] ),
        .R(counter_set));
  CARRY4 \counter_out_reg[4]_i_1 
       (.CI(\counter_out_reg[0]_i_3_n_0 ),
        .CO({\counter_out_reg[4]_i_1_n_0 ,\counter_out_reg[4]_i_1_n_1 ,\counter_out_reg[4]_i_1_n_2 ,\counter_out_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_out_reg[4]_i_1_n_4 ,\counter_out_reg[4]_i_1_n_5 ,\counter_out_reg[4]_i_1_n_6 ,\counter_out_reg[4]_i_1_n_7 }),
        .S({\counter_out_reg_n_0_[7] ,\counter_out_reg_n_0_[6] ,\counter_out_reg_n_0_[5] ,\counter_out_reg_n_0_[4] }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[5] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[4]_i_1_n_6 ),
        .Q(\counter_out_reg_n_0_[5] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[6] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[4]_i_1_n_5 ),
        .Q(\counter_out_reg_n_0_[6] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[7] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[4]_i_1_n_4 ),
        .Q(\counter_out_reg_n_0_[7] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[8] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[8]_i_1_n_7 ),
        .Q(\counter_out_reg_n_0_[8] ),
        .R(counter_set));
  CARRY4 \counter_out_reg[8]_i_1 
       (.CI(\counter_out_reg[4]_i_1_n_0 ),
        .CO({\counter_out_reg[8]_i_1_n_0 ,\counter_out_reg[8]_i_1_n_1 ,\counter_out_reg[8]_i_1_n_2 ,\counter_out_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_out_reg[8]_i_1_n_4 ,\counter_out_reg[8]_i_1_n_5 ,\counter_out_reg[8]_i_1_n_6 ,\counter_out_reg[8]_i_1_n_7 }),
        .S({\counter_out_reg_n_0_[11] ,\counter_out_reg_n_0_[10] ,\counter_out_reg_n_0_[9] ,\counter_out_reg_n_0_[8] }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[9] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\counter_out_reg[8]_i_1_n_6 ),
        .Q(\counter_out_reg_n_0_[9] ),
        .R(counter_set));
  LUT1 #(
    .INIT(2'h1)) 
    \flipflops[0]_i_1 
       (.I0(rot_SW),
        .O(\flipflops[0]_i_1_n_0 ));
  FDRE \flipflops_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\flipflops[0]_i_1_n_0 ),
        .Q(\flipflops_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \flipflops_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\flipflops_reg_n_0_[0] ),
        .Q(p_0_in),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFD80)) 
    result_i_1
       (.I0(counter_out_reg),
        .I1(\flipflops_reg_n_0_[0] ),
        .I2(p_0_in),
        .I3(sw_deb),
        .O(result_i_1_n_0));
  FDRE result_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(result_i_1_n_0),
        .Q(sw_deb),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "debounce" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce__parameterized0
   (AB_deb,
    s00_axi_aclk,
    rot_A);
  output [0:0]AB_deb;
  input s00_axi_aclk;
  input rot_A;

  wire [0:0]AB_deb;
  wire \counter_out[0]_i_2__0_n_0 ;
  wire \counter_out[0]_i_4__0_n_0 ;
  wire [15:15]counter_out_reg;
  wire \counter_out_reg[0]_i_3__0_n_0 ;
  wire \counter_out_reg[0]_i_3__0_n_1 ;
  wire \counter_out_reg[0]_i_3__0_n_2 ;
  wire \counter_out_reg[0]_i_3__0_n_3 ;
  wire \counter_out_reg[0]_i_3__0_n_4 ;
  wire \counter_out_reg[0]_i_3__0_n_5 ;
  wire \counter_out_reg[0]_i_3__0_n_6 ;
  wire \counter_out_reg[0]_i_3__0_n_7 ;
  wire \counter_out_reg[12]_i_1__0_n_1 ;
  wire \counter_out_reg[12]_i_1__0_n_2 ;
  wire \counter_out_reg[12]_i_1__0_n_3 ;
  wire \counter_out_reg[12]_i_1__0_n_4 ;
  wire \counter_out_reg[12]_i_1__0_n_5 ;
  wire \counter_out_reg[12]_i_1__0_n_6 ;
  wire \counter_out_reg[12]_i_1__0_n_7 ;
  wire \counter_out_reg[4]_i_1__0_n_0 ;
  wire \counter_out_reg[4]_i_1__0_n_1 ;
  wire \counter_out_reg[4]_i_1__0_n_2 ;
  wire \counter_out_reg[4]_i_1__0_n_3 ;
  wire \counter_out_reg[4]_i_1__0_n_4 ;
  wire \counter_out_reg[4]_i_1__0_n_5 ;
  wire \counter_out_reg[4]_i_1__0_n_6 ;
  wire \counter_out_reg[4]_i_1__0_n_7 ;
  wire \counter_out_reg[8]_i_1__0_n_0 ;
  wire \counter_out_reg[8]_i_1__0_n_1 ;
  wire \counter_out_reg[8]_i_1__0_n_2 ;
  wire \counter_out_reg[8]_i_1__0_n_3 ;
  wire \counter_out_reg[8]_i_1__0_n_4 ;
  wire \counter_out_reg[8]_i_1__0_n_5 ;
  wire \counter_out_reg[8]_i_1__0_n_6 ;
  wire \counter_out_reg[8]_i_1__0_n_7 ;
  wire \counter_out_reg_n_0_[0] ;
  wire \counter_out_reg_n_0_[10] ;
  wire \counter_out_reg_n_0_[11] ;
  wire \counter_out_reg_n_0_[12] ;
  wire \counter_out_reg_n_0_[13] ;
  wire \counter_out_reg_n_0_[14] ;
  wire \counter_out_reg_n_0_[1] ;
  wire \counter_out_reg_n_0_[2] ;
  wire \counter_out_reg_n_0_[3] ;
  wire \counter_out_reg_n_0_[4] ;
  wire \counter_out_reg_n_0_[5] ;
  wire \counter_out_reg_n_0_[6] ;
  wire \counter_out_reg_n_0_[7] ;
  wire \counter_out_reg_n_0_[8] ;
  wire \counter_out_reg_n_0_[9] ;
  wire counter_set;
  wire \flipflops[0]_i_1__0_n_0 ;
  wire \flipflops_reg_n_0_[0] ;
  wire p_0_in;
  wire result_i_1__1_n_0;
  wire rot_A;
  wire s00_axi_aclk;
  wire [3:3]\NLW_counter_out_reg[12]_i_1__0_CO_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h6)) 
    \counter_out[0]_i_1__0 
       (.I0(p_0_in),
        .I1(\flipflops_reg_n_0_[0] ),
        .O(counter_set));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_out[0]_i_2__0 
       (.I0(counter_out_reg),
        .O(\counter_out[0]_i_2__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_out[0]_i_4__0 
       (.I0(\counter_out_reg_n_0_[0] ),
        .O(\counter_out[0]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__0_n_0 ),
        .D(\counter_out_reg[0]_i_3__0_n_7 ),
        .Q(\counter_out_reg_n_0_[0] ),
        .R(counter_set));
  CARRY4 \counter_out_reg[0]_i_3__0 
       (.CI(1'b0),
        .CO({\counter_out_reg[0]_i_3__0_n_0 ,\counter_out_reg[0]_i_3__0_n_1 ,\counter_out_reg[0]_i_3__0_n_2 ,\counter_out_reg[0]_i_3__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_out_reg[0]_i_3__0_n_4 ,\counter_out_reg[0]_i_3__0_n_5 ,\counter_out_reg[0]_i_3__0_n_6 ,\counter_out_reg[0]_i_3__0_n_7 }),
        .S({\counter_out_reg_n_0_[3] ,\counter_out_reg_n_0_[2] ,\counter_out_reg_n_0_[1] ,\counter_out[0]_i_4__0_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__0_n_0 ),
        .D(\counter_out_reg[8]_i_1__0_n_5 ),
        .Q(\counter_out_reg_n_0_[10] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__0_n_0 ),
        .D(\counter_out_reg[8]_i_1__0_n_4 ),
        .Q(\counter_out_reg_n_0_[11] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__0_n_0 ),
        .D(\counter_out_reg[12]_i_1__0_n_7 ),
        .Q(\counter_out_reg_n_0_[12] ),
        .R(counter_set));
  CARRY4 \counter_out_reg[12]_i_1__0 
       (.CI(\counter_out_reg[8]_i_1__0_n_0 ),
        .CO({\NLW_counter_out_reg[12]_i_1__0_CO_UNCONNECTED [3],\counter_out_reg[12]_i_1__0_n_1 ,\counter_out_reg[12]_i_1__0_n_2 ,\counter_out_reg[12]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_out_reg[12]_i_1__0_n_4 ,\counter_out_reg[12]_i_1__0_n_5 ,\counter_out_reg[12]_i_1__0_n_6 ,\counter_out_reg[12]_i_1__0_n_7 }),
        .S({counter_out_reg,\counter_out_reg_n_0_[14] ,\counter_out_reg_n_0_[13] ,\counter_out_reg_n_0_[12] }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__0_n_0 ),
        .D(\counter_out_reg[12]_i_1__0_n_6 ),
        .Q(\counter_out_reg_n_0_[13] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__0_n_0 ),
        .D(\counter_out_reg[12]_i_1__0_n_5 ),
        .Q(\counter_out_reg_n_0_[14] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__0_n_0 ),
        .D(\counter_out_reg[12]_i_1__0_n_4 ),
        .Q(counter_out_reg),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__0_n_0 ),
        .D(\counter_out_reg[0]_i_3__0_n_6 ),
        .Q(\counter_out_reg_n_0_[1] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__0_n_0 ),
        .D(\counter_out_reg[0]_i_3__0_n_5 ),
        .Q(\counter_out_reg_n_0_[2] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__0_n_0 ),
        .D(\counter_out_reg[0]_i_3__0_n_4 ),
        .Q(\counter_out_reg_n_0_[3] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__0_n_0 ),
        .D(\counter_out_reg[4]_i_1__0_n_7 ),
        .Q(\counter_out_reg_n_0_[4] ),
        .R(counter_set));
  CARRY4 \counter_out_reg[4]_i_1__0 
       (.CI(\counter_out_reg[0]_i_3__0_n_0 ),
        .CO({\counter_out_reg[4]_i_1__0_n_0 ,\counter_out_reg[4]_i_1__0_n_1 ,\counter_out_reg[4]_i_1__0_n_2 ,\counter_out_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_out_reg[4]_i_1__0_n_4 ,\counter_out_reg[4]_i_1__0_n_5 ,\counter_out_reg[4]_i_1__0_n_6 ,\counter_out_reg[4]_i_1__0_n_7 }),
        .S({\counter_out_reg_n_0_[7] ,\counter_out_reg_n_0_[6] ,\counter_out_reg_n_0_[5] ,\counter_out_reg_n_0_[4] }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__0_n_0 ),
        .D(\counter_out_reg[4]_i_1__0_n_6 ),
        .Q(\counter_out_reg_n_0_[5] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__0_n_0 ),
        .D(\counter_out_reg[4]_i_1__0_n_5 ),
        .Q(\counter_out_reg_n_0_[6] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__0_n_0 ),
        .D(\counter_out_reg[4]_i_1__0_n_4 ),
        .Q(\counter_out_reg_n_0_[7] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__0_n_0 ),
        .D(\counter_out_reg[8]_i_1__0_n_7 ),
        .Q(\counter_out_reg_n_0_[8] ),
        .R(counter_set));
  CARRY4 \counter_out_reg[8]_i_1__0 
       (.CI(\counter_out_reg[4]_i_1__0_n_0 ),
        .CO({\counter_out_reg[8]_i_1__0_n_0 ,\counter_out_reg[8]_i_1__0_n_1 ,\counter_out_reg[8]_i_1__0_n_2 ,\counter_out_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_out_reg[8]_i_1__0_n_4 ,\counter_out_reg[8]_i_1__0_n_5 ,\counter_out_reg[8]_i_1__0_n_6 ,\counter_out_reg[8]_i_1__0_n_7 }),
        .S({\counter_out_reg_n_0_[11] ,\counter_out_reg_n_0_[10] ,\counter_out_reg_n_0_[9] ,\counter_out_reg_n_0_[8] }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__0_n_0 ),
        .D(\counter_out_reg[8]_i_1__0_n_6 ),
        .Q(\counter_out_reg_n_0_[9] ),
        .R(counter_set));
  LUT1 #(
    .INIT(2'h1)) 
    \flipflops[0]_i_1__0 
       (.I0(rot_A),
        .O(\flipflops[0]_i_1__0_n_0 ));
  FDRE \flipflops_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\flipflops[0]_i_1__0_n_0 ),
        .Q(\flipflops_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \flipflops_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\flipflops_reg_n_0_[0] ),
        .Q(p_0_in),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFD80)) 
    result_i_1__1
       (.I0(counter_out_reg),
        .I1(\flipflops_reg_n_0_[0] ),
        .I2(p_0_in),
        .I3(AB_deb),
        .O(result_i_1__1_n_0));
  FDRE result_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(result_i_1__1_n_0),
        .Q(AB_deb),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "debounce" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce__parameterized0_0
   (AB_deb,
    \State_reg[0] ,
    s00_axi_aclk,
    rot_B,
    result_reg_0);
  output [0:0]AB_deb;
  output \State_reg[0] ;
  input s00_axi_aclk;
  input rot_B;
  input [0:0]result_reg_0;

  wire [0:0]AB_deb;
  wire \State_reg[0] ;
  wire \counter_out[0]_i_2__1_n_0 ;
  wire \counter_out[0]_i_4__1_n_0 ;
  wire [15:15]counter_out_reg;
  wire \counter_out_reg[0]_i_3__1_n_0 ;
  wire \counter_out_reg[0]_i_3__1_n_1 ;
  wire \counter_out_reg[0]_i_3__1_n_2 ;
  wire \counter_out_reg[0]_i_3__1_n_3 ;
  wire \counter_out_reg[0]_i_3__1_n_4 ;
  wire \counter_out_reg[0]_i_3__1_n_5 ;
  wire \counter_out_reg[0]_i_3__1_n_6 ;
  wire \counter_out_reg[0]_i_3__1_n_7 ;
  wire \counter_out_reg[12]_i_1__1_n_1 ;
  wire \counter_out_reg[12]_i_1__1_n_2 ;
  wire \counter_out_reg[12]_i_1__1_n_3 ;
  wire \counter_out_reg[12]_i_1__1_n_4 ;
  wire \counter_out_reg[12]_i_1__1_n_5 ;
  wire \counter_out_reg[12]_i_1__1_n_6 ;
  wire \counter_out_reg[12]_i_1__1_n_7 ;
  wire \counter_out_reg[4]_i_1__1_n_0 ;
  wire \counter_out_reg[4]_i_1__1_n_1 ;
  wire \counter_out_reg[4]_i_1__1_n_2 ;
  wire \counter_out_reg[4]_i_1__1_n_3 ;
  wire \counter_out_reg[4]_i_1__1_n_4 ;
  wire \counter_out_reg[4]_i_1__1_n_5 ;
  wire \counter_out_reg[4]_i_1__1_n_6 ;
  wire \counter_out_reg[4]_i_1__1_n_7 ;
  wire \counter_out_reg[8]_i_1__1_n_0 ;
  wire \counter_out_reg[8]_i_1__1_n_1 ;
  wire \counter_out_reg[8]_i_1__1_n_2 ;
  wire \counter_out_reg[8]_i_1__1_n_3 ;
  wire \counter_out_reg[8]_i_1__1_n_4 ;
  wire \counter_out_reg[8]_i_1__1_n_5 ;
  wire \counter_out_reg[8]_i_1__1_n_6 ;
  wire \counter_out_reg[8]_i_1__1_n_7 ;
  wire \counter_out_reg_n_0_[0] ;
  wire \counter_out_reg_n_0_[10] ;
  wire \counter_out_reg_n_0_[11] ;
  wire \counter_out_reg_n_0_[12] ;
  wire \counter_out_reg_n_0_[13] ;
  wire \counter_out_reg_n_0_[14] ;
  wire \counter_out_reg_n_0_[1] ;
  wire \counter_out_reg_n_0_[2] ;
  wire \counter_out_reg_n_0_[3] ;
  wire \counter_out_reg_n_0_[4] ;
  wire \counter_out_reg_n_0_[5] ;
  wire \counter_out_reg_n_0_[6] ;
  wire \counter_out_reg_n_0_[7] ;
  wire \counter_out_reg_n_0_[8] ;
  wire \counter_out_reg_n_0_[9] ;
  wire counter_set;
  wire \flipflops[0]_i_1__1_n_0 ;
  wire \flipflops_reg_n_0_[0] ;
  wire p_0_in;
  wire result_i_1__0_n_0;
  wire [0:0]result_reg_0;
  wire rot_B;
  wire s00_axi_aclk;
  wire [3:3]\NLW_counter_out_reg[12]_i_1__1_CO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \State[0]_i_1 
       (.I0(AB_deb),
        .I1(result_reg_0),
        .O(\State_reg[0] ));
  LUT2 #(
    .INIT(4'h6)) 
    \counter_out[0]_i_1__1 
       (.I0(p_0_in),
        .I1(\flipflops_reg_n_0_[0] ),
        .O(counter_set));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_out[0]_i_2__1 
       (.I0(counter_out_reg),
        .O(\counter_out[0]_i_2__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_out[0]_i_4__1 
       (.I0(\counter_out_reg_n_0_[0] ),
        .O(\counter_out[0]_i_4__1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__1_n_0 ),
        .D(\counter_out_reg[0]_i_3__1_n_7 ),
        .Q(\counter_out_reg_n_0_[0] ),
        .R(counter_set));
  CARRY4 \counter_out_reg[0]_i_3__1 
       (.CI(1'b0),
        .CO({\counter_out_reg[0]_i_3__1_n_0 ,\counter_out_reg[0]_i_3__1_n_1 ,\counter_out_reg[0]_i_3__1_n_2 ,\counter_out_reg[0]_i_3__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_out_reg[0]_i_3__1_n_4 ,\counter_out_reg[0]_i_3__1_n_5 ,\counter_out_reg[0]_i_3__1_n_6 ,\counter_out_reg[0]_i_3__1_n_7 }),
        .S({\counter_out_reg_n_0_[3] ,\counter_out_reg_n_0_[2] ,\counter_out_reg_n_0_[1] ,\counter_out[0]_i_4__1_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__1_n_0 ),
        .D(\counter_out_reg[8]_i_1__1_n_5 ),
        .Q(\counter_out_reg_n_0_[10] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__1_n_0 ),
        .D(\counter_out_reg[8]_i_1__1_n_4 ),
        .Q(\counter_out_reg_n_0_[11] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__1_n_0 ),
        .D(\counter_out_reg[12]_i_1__1_n_7 ),
        .Q(\counter_out_reg_n_0_[12] ),
        .R(counter_set));
  CARRY4 \counter_out_reg[12]_i_1__1 
       (.CI(\counter_out_reg[8]_i_1__1_n_0 ),
        .CO({\NLW_counter_out_reg[12]_i_1__1_CO_UNCONNECTED [3],\counter_out_reg[12]_i_1__1_n_1 ,\counter_out_reg[12]_i_1__1_n_2 ,\counter_out_reg[12]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_out_reg[12]_i_1__1_n_4 ,\counter_out_reg[12]_i_1__1_n_5 ,\counter_out_reg[12]_i_1__1_n_6 ,\counter_out_reg[12]_i_1__1_n_7 }),
        .S({counter_out_reg,\counter_out_reg_n_0_[14] ,\counter_out_reg_n_0_[13] ,\counter_out_reg_n_0_[12] }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__1_n_0 ),
        .D(\counter_out_reg[12]_i_1__1_n_6 ),
        .Q(\counter_out_reg_n_0_[13] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__1_n_0 ),
        .D(\counter_out_reg[12]_i_1__1_n_5 ),
        .Q(\counter_out_reg_n_0_[14] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__1_n_0 ),
        .D(\counter_out_reg[12]_i_1__1_n_4 ),
        .Q(counter_out_reg),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__1_n_0 ),
        .D(\counter_out_reg[0]_i_3__1_n_6 ),
        .Q(\counter_out_reg_n_0_[1] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__1_n_0 ),
        .D(\counter_out_reg[0]_i_3__1_n_5 ),
        .Q(\counter_out_reg_n_0_[2] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__1_n_0 ),
        .D(\counter_out_reg[0]_i_3__1_n_4 ),
        .Q(\counter_out_reg_n_0_[3] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__1_n_0 ),
        .D(\counter_out_reg[4]_i_1__1_n_7 ),
        .Q(\counter_out_reg_n_0_[4] ),
        .R(counter_set));
  CARRY4 \counter_out_reg[4]_i_1__1 
       (.CI(\counter_out_reg[0]_i_3__1_n_0 ),
        .CO({\counter_out_reg[4]_i_1__1_n_0 ,\counter_out_reg[4]_i_1__1_n_1 ,\counter_out_reg[4]_i_1__1_n_2 ,\counter_out_reg[4]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_out_reg[4]_i_1__1_n_4 ,\counter_out_reg[4]_i_1__1_n_5 ,\counter_out_reg[4]_i_1__1_n_6 ,\counter_out_reg[4]_i_1__1_n_7 }),
        .S({\counter_out_reg_n_0_[7] ,\counter_out_reg_n_0_[6] ,\counter_out_reg_n_0_[5] ,\counter_out_reg_n_0_[4] }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__1_n_0 ),
        .D(\counter_out_reg[4]_i_1__1_n_6 ),
        .Q(\counter_out_reg_n_0_[5] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__1_n_0 ),
        .D(\counter_out_reg[4]_i_1__1_n_5 ),
        .Q(\counter_out_reg_n_0_[6] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__1_n_0 ),
        .D(\counter_out_reg[4]_i_1__1_n_4 ),
        .Q(\counter_out_reg_n_0_[7] ),
        .R(counter_set));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__1_n_0 ),
        .D(\counter_out_reg[8]_i_1__1_n_7 ),
        .Q(\counter_out_reg_n_0_[8] ),
        .R(counter_set));
  CARRY4 \counter_out_reg[8]_i_1__1 
       (.CI(\counter_out_reg[4]_i_1__1_n_0 ),
        .CO({\counter_out_reg[8]_i_1__1_n_0 ,\counter_out_reg[8]_i_1__1_n_1 ,\counter_out_reg[8]_i_1__1_n_2 ,\counter_out_reg[8]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_out_reg[8]_i_1__1_n_4 ,\counter_out_reg[8]_i_1__1_n_5 ,\counter_out_reg[8]_i_1__1_n_6 ,\counter_out_reg[8]_i_1__1_n_7 }),
        .S({\counter_out_reg_n_0_[11] ,\counter_out_reg_n_0_[10] ,\counter_out_reg_n_0_[9] ,\counter_out_reg_n_0_[8] }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_out_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\counter_out[0]_i_2__1_n_0 ),
        .D(\counter_out_reg[8]_i_1__1_n_6 ),
        .Q(\counter_out_reg_n_0_[9] ),
        .R(counter_set));
  LUT1 #(
    .INIT(2'h1)) 
    \flipflops[0]_i_1__1 
       (.I0(rot_B),
        .O(\flipflops[0]_i_1__1_n_0 ));
  FDRE \flipflops_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\flipflops[0]_i_1__1_n_0 ),
        .Q(\flipflops_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \flipflops_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\flipflops_reg_n_0_[0] ),
        .Q(p_0_in),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hFD80)) 
    result_i_1__0
       (.I0(counter_out_reg),
        .I1(\flipflops_reg_n_0_[0] ),
        .I2(p_0_in),
        .I3(AB_deb),
        .O(result_i_1__0_n_0));
  FDRE result_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(result_i_1__0_n_0),
        .Q(AB_deb),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_rotary_decoder_0_0,rotary_decoder_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "rotary_decoder_v1_0,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (rot_A,
    rot_B,
    rot_SW,
    ISR_rot,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  input rot_A;
  input rot_B;
  input rot_SW;
  output ISR_rot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 50000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 4, NUM_WRITE_THREADS 4, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [3:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [3:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire ISR_rot;
  wire rot_A;
  wire rot_B;
  wire rot_SW;
  wire s00_axi_aclk;
  wire [3:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [3:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rotary_decoder_v1_0 U0
       (.ISR_rot(ISR_rot),
        .S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .rot_A(rot_A),
        .rot_B(rot_B),
        .rot_SW(rot_SW),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[3:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[3:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rotary_decoder_v1_0
   (S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    s00_axi_rvalid,
    s00_axi_rdata,
    ISR_rot,
    s00_axi_bvalid,
    s00_axi_aclk,
    s00_axi_arvalid,
    s00_axi_araddr,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wdata,
    s00_axi_wstrb,
    rot_SW,
    rot_A,
    rot_B,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output s00_axi_rvalid;
  output [31:0]s00_axi_rdata;
  output ISR_rot;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input s00_axi_arvalid;
  input [1:0]s00_axi_araddr;
  input [1:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_wstrb;
  input rot_SW;
  input rot_A;
  input rot_B;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire ISR_rot;
  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire rot_A;
  wire rot_B;
  wire rot_SW;
  wire s00_axi_aclk;
  wire [1:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [1:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rotary_decoder_v1_0_S00_AXI rotary_decoder_v1_0_S00_AXI_inst
       (.ISR_rot(ISR_rot),
        .S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .rot_A(rot_A),
        .rot_B(rot_B),
        .rot_SW(rot_SW),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rotary_decoder_v1_0_S00_AXI
   (S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    s00_axi_rvalid,
    s00_axi_rdata,
    ISR_rot,
    s00_axi_bvalid,
    s00_axi_aclk,
    s00_axi_arvalid,
    s00_axi_araddr,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wdata,
    s00_axi_wstrb,
    rot_SW,
    rot_A,
    rot_B,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output s00_axi_rvalid;
  output [31:0]s00_axi_rdata;
  output ISR_rot;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input s00_axi_arvalid;
  input [1:0]s00_axi_araddr;
  input [1:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_wstrb;
  input rot_SW;
  input rot_A;
  input rot_B;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire ISR_rot;
  wire SW_rise_latch_q;
  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire [1:0]UD_latching_q;
  wire [3:2]axi_araddr;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire axi_arready0;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_awready_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire clear_latch;
  wire [1:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out;
  wire rot_A;
  wire rot_B;
  wire rot_SW;
  wire s00_axi_aclk;
  wire [1:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [1:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [31:0]slv_reg1;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire [31:0]slv_reg2;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg2[7]_i_1_n_0 ;
  wire [31:0]slv_reg3;
  wire slv_reg_wren__0;

  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(s00_axi_araddr[0]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(axi_araddr[2]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(s00_axi_araddr[1]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(axi_araddr[3]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(axi_araddr[2]),
        .S(axi_awready_i_1_n_0));
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(axi_araddr[3]),
        .S(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(axi_awready_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \axi_awaddr[2]_i_1 
       (.I0(s00_axi_awaddr[0]),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_AWREADY),
        .I4(p_0_in[0]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \axi_awaddr[3]_i_1 
       (.I0(s00_axi_awaddr[1]),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_AWREADY),
        .I4(p_0_in[1]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(p_0_in[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(p_0_in[1]),
        .R(axi_awready_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_2
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awvalid),
        .I2(S_AXI_AWREADY),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awvalid),
        .I2(S_AXI_AWREADY),
        .I3(S_AXI_WREADY),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[0]_i_1 
       (.I0(slv_reg1[0]),
        .I1(SW_rise_latch_q),
        .I2(slv_reg3[0]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[0]),
        .O(reg_data_out[0]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[10]_i_1 
       (.I0(slv_reg1[10]),
        .I1(slv_reg3[10]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[10]),
        .O(reg_data_out[10]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[11]_i_1 
       (.I0(slv_reg1[11]),
        .I1(slv_reg3[11]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[11]),
        .O(reg_data_out[11]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[12]_i_1 
       (.I0(slv_reg1[12]),
        .I1(slv_reg3[12]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[12]),
        .O(reg_data_out[12]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[13]_i_1 
       (.I0(slv_reg1[13]),
        .I1(slv_reg3[13]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[13]),
        .O(reg_data_out[13]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[14]_i_1 
       (.I0(slv_reg1[14]),
        .I1(slv_reg3[14]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[14]),
        .O(reg_data_out[14]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[15]_i_1 
       (.I0(slv_reg1[15]),
        .I1(slv_reg3[15]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[15]),
        .O(reg_data_out[15]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[16]_i_1 
       (.I0(slv_reg1[16]),
        .I1(slv_reg3[16]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[16]),
        .O(reg_data_out[16]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[17]_i_1 
       (.I0(slv_reg1[17]),
        .I1(slv_reg3[17]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[17]),
        .O(reg_data_out[17]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[18]_i_1 
       (.I0(slv_reg1[18]),
        .I1(slv_reg3[18]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[18]),
        .O(reg_data_out[18]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[19]_i_1 
       (.I0(slv_reg1[19]),
        .I1(slv_reg3[19]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[19]),
        .O(reg_data_out[19]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[1]_i_1 
       (.I0(slv_reg1[1]),
        .I1(slv_reg3[1]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[1]),
        .O(reg_data_out[1]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[20]_i_1 
       (.I0(slv_reg1[20]),
        .I1(slv_reg3[20]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[20]),
        .O(reg_data_out[20]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[21]_i_1 
       (.I0(slv_reg1[21]),
        .I1(slv_reg3[21]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[21]),
        .O(reg_data_out[21]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[22]_i_1 
       (.I0(slv_reg1[22]),
        .I1(slv_reg3[22]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[22]),
        .O(reg_data_out[22]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[23]_i_1 
       (.I0(slv_reg1[23]),
        .I1(slv_reg3[23]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[23]),
        .O(reg_data_out[23]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[24]_i_1 
       (.I0(slv_reg1[24]),
        .I1(slv_reg3[24]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[24]),
        .O(reg_data_out[24]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[25]_i_1 
       (.I0(slv_reg1[25]),
        .I1(slv_reg3[25]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[25]),
        .O(reg_data_out[25]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[26]_i_1 
       (.I0(slv_reg1[26]),
        .I1(slv_reg3[26]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[26]),
        .O(reg_data_out[26]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[27]_i_1 
       (.I0(slv_reg1[27]),
        .I1(slv_reg3[27]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[27]),
        .O(reg_data_out[27]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[28]_i_1 
       (.I0(slv_reg1[28]),
        .I1(slv_reg3[28]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[28]),
        .O(reg_data_out[28]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[29]_i_1 
       (.I0(slv_reg1[29]),
        .I1(slv_reg3[29]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[29]),
        .O(reg_data_out[29]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[2]_i_1 
       (.I0(slv_reg1[2]),
        .I1(slv_reg3[2]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[2]),
        .O(reg_data_out[2]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[30]_i_1 
       (.I0(slv_reg1[30]),
        .I1(slv_reg3[30]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[30]),
        .O(reg_data_out[30]));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .O(clear_latch));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[31]_i_2 
       (.I0(slv_reg1[31]),
        .I1(slv_reg3[31]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[31]),
        .O(reg_data_out[31]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[3]_i_1 
       (.I0(slv_reg1[3]),
        .I1(slv_reg3[3]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[3]),
        .O(reg_data_out[3]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[4]_i_1 
       (.I0(slv_reg1[4]),
        .I1(UD_latching_q[0]),
        .I2(slv_reg3[4]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[4]),
        .O(reg_data_out[4]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[5]_i_1 
       (.I0(slv_reg1[5]),
        .I1(UD_latching_q[1]),
        .I2(slv_reg3[5]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[5]),
        .O(reg_data_out[5]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[6]_i_1 
       (.I0(slv_reg1[6]),
        .I1(slv_reg3[6]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[6]),
        .O(reg_data_out[6]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[7]_i_1 
       (.I0(slv_reg1[7]),
        .I1(slv_reg3[7]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[7]),
        .O(reg_data_out[7]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[8]_i_1 
       (.I0(slv_reg1[8]),
        .I1(slv_reg3[8]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[8]),
        .O(reg_data_out[8]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[9]_i_1 
       (.I0(slv_reg1[9]),
        .I1(slv_reg3[9]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[9]),
        .O(reg_data_out[9]));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(clear_latch),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awvalid),
        .I2(S_AXI_WREADY),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(axi_awready_i_1_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rotary_top rotlogic
       (.ISR_rot(ISR_rot),
        .SW_rise_latch_q(SW_rise_latch_q),
        .UD_latching_q(UD_latching_q),
        .axi_arready_reg(S_AXI_ARREADY),
        .axi_rvalid_reg(s00_axi_rvalid),
        .rot_A(rot_A),
        .rot_B(rot_B),
        .rot_SW(rot_SW),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_arvalid(s00_axi_arvalid));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[1]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[2]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[3]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[0]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg1[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg1[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg1[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg1[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg1[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg1[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg1[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg1[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg1[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg1[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg1[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg1[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg1[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg1[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg1[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg1[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg1[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg1[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg1[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg1[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg1[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[1]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[2]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[3]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(s00_axi_wstrb[0]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[7]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg2[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg2[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg2[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg2[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg2[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg2[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg2[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg2[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg2[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg2[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg2[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg2[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg2[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg2[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg2[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg2[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg2[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg2[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg2[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg2[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg2[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg2[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg2[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg2[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg2[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg2[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg2[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg2[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg2[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg2[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg2[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg2[9]),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(p_1_in[15]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(p_1_in[23]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(p_1_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[31]_i_2 
       (.I0(S_AXI_WREADY),
        .I1(S_AXI_AWREADY),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .O(slv_reg_wren__0));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(p_1_in[7]));
  FDRE \slv_reg3_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg3[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg3[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg3[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg3[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg3[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg3[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg3[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg3[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg3[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg3[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg3[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg3[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg3[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg3[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg3[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg3[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg3[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg3[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg3[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg3[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg3[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg3[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg3[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg3[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg3[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg3[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg3[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg3[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg3[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg3[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg3[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg3[9]),
        .R(axi_awready_i_1_n_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rotary_top
   (SW_rise_latch_q,
    ISR_rot,
    UD_latching_q,
    s00_axi_aclk,
    axi_rvalid_reg,
    axi_arready_reg,
    s00_axi_arvalid,
    rot_SW,
    rot_A,
    rot_B);
  output SW_rise_latch_q;
  output ISR_rot;
  output [1:0]UD_latching_q;
  input s00_axi_aclk;
  input axi_rvalid_reg;
  input axi_arready_reg;
  input s00_axi_arvalid;
  input rot_SW;
  input rot_A;
  input rot_B;

  wire [1:0]AB_deb;
  wire ISR_rot;
  wire SW_rise;
  wire SW_rise0_n_0;
  wire SW_rise_latch_q;
  wire [1:0]UD_latching_q;
  wire \UD_latching_q[0]_i_1_n_0 ;
  wire \UD_latching_q[1]_i_1_n_0 ;
  wire [1:0]UD_sig;
  wire axi_arready_reg;
  wire axi_rvalid_reg;
  wire debb_n_1;
  wire debsw_n_1;
  wire rot_A;
  wire rot_B;
  wire rot_SW;
  wire s00_axi_aclk;
  wire s00_axi_arvalid;
  wire sw_deb;
  wire sw_deb_delay;

  LUT2 #(
    .INIT(4'h4)) 
    SW_rise0
       (.I0(sw_deb_delay),
        .I1(sw_deb),
        .O(SW_rise0_n_0));
  FDRE SW_rise_latch_q_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(debsw_n_1),
        .Q(SW_rise_latch_q),
        .R(1'b0));
  FDRE SW_rise_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(SW_rise0_n_0),
        .Q(SW_rise),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hCE00CECECECECECE)) 
    \UD_latching_q[0]_i_1 
       (.I0(UD_latching_q[0]),
        .I1(UD_sig[0]),
        .I2(UD_sig[1]),
        .I3(axi_rvalid_reg),
        .I4(axi_arready_reg),
        .I5(s00_axi_arvalid),
        .O(\UD_latching_q[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF200F2F2F2F2F2F2)) 
    \UD_latching_q[1]_i_1 
       (.I0(UD_latching_q[1]),
        .I1(UD_sig[0]),
        .I2(UD_sig[1]),
        .I3(axi_rvalid_reg),
        .I4(axi_arready_reg),
        .I5(s00_axi_arvalid),
        .O(\UD_latching_q[1]_i_1_n_0 ));
  FDRE \UD_latching_q_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\UD_latching_q[0]_i_1_n_0 ),
        .Q(UD_latching_q[0]),
        .R(1'b0));
  FDRE \UD_latching_q_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\UD_latching_q[1]_i_1_n_0 ),
        .Q(UD_latching_q[1]),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce__parameterized0 deba
       (.AB_deb(AB_deb[1]),
        .rot_A(rot_A),
        .s00_axi_aclk(s00_axi_aclk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce__parameterized0_0 debb
       (.AB_deb(AB_deb[0]),
        .\State_reg[0] (debb_n_1),
        .result_reg_0(AB_deb[1]),
        .rot_B(rot_B),
        .s00_axi_aclk(s00_axi_aclk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce debsw
       (.SW_rise_latch_q(SW_rise_latch_q),
        .SW_rise_latch_q_reg(debsw_n_1),
        .axi_arready_reg(axi_arready_reg),
        .axi_rvalid_reg(axi_rvalid_reg),
        .rot_SW(rot_SW),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_arvalid(s00_axi_arvalid),
        .sw_deb(sw_deb),
        .sw_deb_delay(sw_deb_delay));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_x1enc2 enc
       (.AB_deb(AB_deb),
        .ISR_rot(ISR_rot),
        .Q(UD_sig),
        .SW_rise(SW_rise),
        .result_reg(debb_n_1),
        .s00_axi_aclk(s00_axi_aclk));
  FDRE sw_deb_delay_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sw_deb),
        .Q(sw_deb_delay),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_x1enc2
   (ISR_rot,
    Q,
    AB_deb,
    s00_axi_aclk,
    result_reg,
    SW_rise);
  output ISR_rot;
  output [1:0]Q;
  input [1:0]AB_deb;
  input s00_axi_aclk;
  input result_reg;
  input SW_rise;

  wire [1:0]AB_deb;
  wire ISR_rot;
  wire [1:0]Q;
  wire SW_rise;
  wire [1:0]State;
  wire \State[1]_i_1_n_0 ;
  wire \UD[1]_i_1_n_0 ;
  wire \UD_inferred__1/i__n_0 ;
  wire result_reg;
  wire s00_axi_aclk;

  LUT3 #(
    .INIT(8'hFE)) 
    ISR_rot_INST_0
       (.I0(SW_rise),
        .I1(Q[0]),
        .I2(Q[1]),
        .O(ISR_rot));
  LUT4 #(
    .INIT(16'hEB82)) 
    \State[1]_i_1 
       (.I0(State[1]),
        .I1(State[0]),
        .I2(AB_deb[1]),
        .I3(AB_deb[0]),
        .O(\State[1]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "iSTATE:01,iSTATE0:11,iSTATE1:10,iSTATE2:00" *) 
  FDRE #(
    .INIT(1'b0)) 
    \State_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(result_reg),
        .Q(State[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "iSTATE:01,iSTATE0:11,iSTATE1:10,iSTATE2:00" *) 
  FDRE #(
    .INIT(1'b0)) 
    \State_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\State[1]_i_1_n_0 ),
        .Q(State[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    \UD[1]_i_1 
       (.I0(State[0]),
        .I1(AB_deb[0]),
        .I2(AB_deb[1]),
        .I3(State[1]),
        .O(\UD[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \UD_inferred__1/i_ 
       (.I0(State[1]),
        .I1(State[0]),
        .I2(AB_deb[0]),
        .I3(AB_deb[1]),
        .O(\UD_inferred__1/i__n_0 ));
  FDRE \UD_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\UD_inferred__1/i__n_0 ),
        .Q(Q[0]),
        .R(1'b0));
  FDRE \UD_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\UD[1]_i_1_n_0 ),
        .Q(Q[1]),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
